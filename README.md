# renderJsonAsHtml

A simple script to render a lot of JSON data files through some handlebars templates to produce a set of static
HTML pages. It attempts to only render files that haven't changed based upon the JSON file, Template and HTML's
last modified timestamps.

## Installation
To install renderJsonAsHtml locally simply use:
```
npm install render-json-as-html
```

To install it globally then use the following with an account with permissions to install globally:

```
npm install -g render-json-as-html
```

## Usage
The following command line options are available:

verbose
: Turns on verbose logging (default off)

recursive
: Recurse into directories in the JSON data directory rather than just doing the files at the top level of
the JSON directory (default off)

srcDir
: Specifies the JSON data files source directory (default jsonData)

outputDir
: Specifies the directory to put the rendered HTML pages into (default public)

templateDir
: Specifies the directory storing the handlebar templates you wish to use (default templates)

assetDir
: Specifies the directory storing any assets (css, images, etc) required by the templates (default assets)

global
: You can use this to specify additional key value pairs that be available for use in the templates
under the _global object (multiple occurances allowed)

## JSON Data format
The JSON data in the source directory can be any structure you like, but there are two optional properties
that you can set in the JSON data to influence how renderJsonAsHtml will operate:

\_template
: this can be used to specify the template that you wish to use to render data

\_render
: if this is set to `false` then the data won't be rendered (that's the boolean value false, not the
string "false").

## Additional handlebars helpers
The handlebars engine has the following additional helpers added

renderMarkdown
: This will process the field as markdown and insert the resulting HTML into the template

slugify
: This will convert the field to a slug format (lowercase alphanumnerics separated by hyphens).

The handlebars engine also has the following additional logic helpers, use them in a subexpression in an if.
E.g. `{{#if (eq name "fred")}} Welcome Fred {{/if}}`

(eq parameter1 parameter2)
: checks if parameter1 equals parameter2

(ne parameter1 parameter2)
: checks if parameter1 is not equal to parameter2

(lt parameter1 parameter2)
: checks if parameter1 is less than parameter2

(gt parameter1 parameter2)
: checks if parameter1 is greater than parameter2

(lte parameter1 parameter2)
: checks if parameter1 is less than or equal to parameter2

(gte parameter1 parameter2)
: checks if parameter1 is greater than or equal to parameter2

(and parameter1 parameter2)
: checks if both of its parameters are true

(or parameter1 parameter2)
: checks if either of its parameters are true

(not parameter1)
: negates parameter1
