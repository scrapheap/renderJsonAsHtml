#!/usr/bin/env node

"use strict";

var handlebars = require('handlebars');
var commandLineArgs = require('command-line-args');
var fs = require('fs-extra');
var path = require('path');
var recursiveReaddirSync = require('recursive-readdir-sync');
var mkdirp = require('mkdirp');
var marked = require('marked');
var fixNewlinesInJsonStrings = require('fix-newlines-in-json-strings');

handlebars.registerHelper({
    renderMarkdown: function (field) {
        var html = ""
        if (field) {
            html = marked(field);
        }
        return new handlebars.SafeString(html);
    },
    slugify: function (field) {
        var slug = "";

        if (field) {
            slug = field.toLowerCase()
                .replace(/[^a-z0-9]/gi, "-")
                .replace(/--+/, "-")
                .replace(/^-+/, "")
                .replace(/-+$/, "");
        }

        return slug;
    },
    eq: function (v1, v2) { return v1 === v2; },
    ne: function (v1, v2) { return v1 !== v2; },
    lt: function (v1, v2) { return v1 < v2; },
    gt: function (v1, v2) { return v1 > v2; },
    lte: function (v1, v2) { return v1 <= v2; },
    gte: function (v1, v2) { return v1 >= v2; },
    and: function (v1, v2) { return v1 && v2; },
    or: function (v1, v2) { return v1 || v2; },
    not: function (v1) { return !v1; }
});

const optionDefinitions = [
    { name: 'verbose', alias: 'v', type: Boolean, defaultValue: false },
    { name: 'recursive', alias: 'r', type: Boolean, defaultValue: false },
    { name: 'srcDir', alias: 's', type: String, defaultValue: 'jsonData' },
    { name: 'outputDir', alias: 'o', type: String, defaultValue: 'public' },
    { name: 'templateDir', alias: 't', type: String, defaultValue: 'templates' },
    { name: 'assetDir', alias: 'a', type: String, defaultValue: 'assets' },
    { name: 'global', alias: 'g', type: String, multiple: true }
];

const options = commandLineArgs(optionDefinitions);

var globals = parseGlobals(options.global);

var verbose = function () {};

// Set up verbose function
if (options.verbose) {
    verbose = function (message) {
        console.log(message);
    };
}

function error(msg) {
    console.log(msg);
}

var directoryContents = fs.readdirSync;

if (options.recursive) {
    directoryContents = function (dir) {
        dir = dir.replace(/\/+$/, "");
        var strip = new RegExp('^' + dir + '\/*');
        return recursiveReaddirSync(dir).map(function (filePath) {
            return filePath.replace(strip, "");
        });
    };
}

// Load all templates and register them all as partials with spaces replaced with underscores
var templates = loadTemplates(options.templateDir);

var outputTimestamps = loadFileTimestamps(options.outputDir);

processAssets(options.assetDir, options.outputDir, outputTimestamps);

processJsonFiles(options.srcDir, options.outputDir, outputTimestamps, templates);



function loadTemplates(templateDir) {
    var templates = {};

    directoryContents(templateDir).forEach(function (file) {
        var filePath = path.join(templateDir, file);
        var templateName = file.replace(/\.[^.]+$/, "");
        var template = {
        }

        var templateStat = fs.statSync(filePath);
        if (templateStat.isDirectory()) {
            return;
        }

        template.lastModified = templateStat.mtime;

        template.sourceContent = fs.readFileSync(filePath, {encoding: 'utf8'});
        template.render = handlebars.compile(template.sourceContent);

        templates[templateName] = template;
        handlebars.registerPartial(templateName, template.render);
    });

    for (var templateName in templates) {
        if (templates.hasOwnProperty(templateName)) {
            templates[templateName].lastModified = lastModifiedPartialTemplate(templateName, templates);
        }
    }

    return templates;
}


function lastModifiedPartialTemplate(templateName, templates, templatesChecked) {
    if (!templatesChecked) {
        templatesChecked = {};
    }

    var lastModified = templates[templateName].lastModified;

    if (!templatesChecked[templateName]) {
        templatesChecked[templateName] = true;

        var partials = templates[templateName].sourceContent.match(/\{\{\s*>\s*([^ }]*)/);

        if (partials) {
            partials.shift();
            partials.forEach(function (partialName) {
                var partialLastModified = lastModifiedPartialTemplate(partialName, templates, templatesChecked)
                if (partialLastModified > lastModified) {
                    lastModified = partialLastModified;
                }
            });
        }
    }

    return lastModified;
}


function loadFileTimestamps(dir) {
    var timestamps = {};

    directoryContents(dir).forEach(function (file) {
        var filePath = path.join(dir, file);
        var fileStat = fs.statSync(filePath);

        if (fileStat.isDirectory()) {
            return;
        }

        timestamps[file] = fileStat.mtime;
    });

    return timestamps;
}

function processAssets(assetDir, outputDir, outputTimestamps) {
    directoryContents(assetDir).forEach(function (file) {
        var filePath = path.join(assetDir, file);
        var targetPath = path.join(outputDir, file);
        var fileStat = fs.statSync(filePath);

        if (fileStat.isDirectory()) {
            return;
        }

        if (!fs.existsSync(path.dirname(targetPath))) {
            mkdirp.sync(path.dirname(targetPath));
        }

        if (!outputTimestamps[file] || outputTimestamps[file] < fileStat.mtime) {
            verbose('copying ' + filePath + ' to ' + targetPath);
            fs.copySync(filePath, targetPath);
        } else {
            verbose('ignoring ' + filePath + ' as ' + targetPath + ' is newer');
        }
    });
}

function processJsonFiles(srcDir, outputDir, outputTimestamps, templates) {
    directoryContents(srcDir).forEach(function (file) {
        var filePath = path.join(srcDir, file);
        var targetFile = file.replace(/\.json$/, ".html");
        var targetPath = path.join(outputDir, targetFile);
        var fileStat = fs.statSync(filePath);

        if (fileStat.isDirectory()) {
            return;
        }
        var json = fs.readFileSync(filePath, 'utf8');
        var fixedJson = fixNewlinesInJsonStrings(json);
        var context = JSON.parse(fixedJson);
        if (context._render === false) {
            verbose('rendering disabled for: ' + filePath); 
            return;
        }

        context._global = globals;

        var template = context._template || 'default';

        if (!templates[template]) {
            error('unknown template: ' + template + ' in ' + filePath);
        }
 
        if (!fs.existsSync(path.dirname(targetPath))) {
            mkdirp.sync(path.dirname(targetPath));
        }

        if (!outputTimestamps[targetFile] || outputTimestamps[targetFile] < fileStat.mtime ||
            outputTimestamps[targetFile] < templates[template].lastModified) {
            verbose('rendering ' + filePath + ' as ' + targetPath);
            var html = templates[template].render(context);
            fs.writeFileSync(targetPath, html);
        } else {
            verbose('ignoring ' + filePath + ' as ' + targetPath + ' is newer');
        }
    });
}

function parseGlobals(globalValues) {
    var globals = {};

    if (globalValues) {
        globalValues.forEach(function (global) {
            var parts = global.split(/:/);
            globals[parts[0]] = parts[1];
        });
    }

    return globals;
}
